<?php

$databases['default']['default'] = [
  'database' => $_SERVER['DATABASE_NAME'],
  'username' => $_SERVER['DATABASE_USER'],
  'password' =>  $_SERVER['DATABASE_PASSWORD'],
  'host' => $_SERVER['DATABASE_HOST'],
  'port' =>  $_SERVER['DATABASE_PORT'],
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

