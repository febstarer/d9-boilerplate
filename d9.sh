#!/bin/sh
rm -rf .gitignore
rm -rf .git
mv gitignore .gitignore

echo "*********************************"
echo "***** Drupal 9 Boilerplate Script"
echo "*********************************"

echo ""
echo "**********************"
echo "Project name?"
read projectname

echo "**********************"
echo "Set up ddev environment"
echo "**********************"
ddev config --project-name=$projectname --project-type=drupal9 --docroot=web --create-docroot --project-tld=test --composer-version=2
ddev start

echo "**********************"
echo "Install composer dependencies"
echo "**********************"
ddev composer install

echo "**********************"
echo "Install site and set default admin theme"
echo "**********************"
ddev drush site:install -y --account-name=omitsis --account-mail=drupal@omitsis.com --site-name=$projectname
ddev drush theme:enable gin
ddev drush config:set system.theme admin gin -y

echo "**********************"
echo "Enable default core modules"
echo "**********************"
ddev drush en -y media media_library syslog language locale content_translation config_translation ckeditor5 responsive_image

echo "**********************"
echo "Disable unnecessary core modules"
echo "**********************"
ddev drush pm:uninstall history color tour

echo "**********************"
echo "Enable default contrib modules"
echo "**********************"
ddev drush en -y drd_agent pathauto admin_toolbar admin_toolbar_tools admin_toolbar_links_access_filter gin_toolbar gin_login field_formatter field_group paragraphs twig_tweak
ddev drush en -y metatag metatag_open_graph metatag_twitter_cards metatag_hreflang redirect simple_sitemap simple_sitemap_engines imageapi_optimize imageapi_optimize_resmushit webp masquerade

echo "**********************"
echo "Load settings.[environment].php code to settings.php"
echo "**********************"
cat << EOF >> web/sites/default/settings.php

// TEMP FILES FOLDER
\$settings["file_temp_path"] = "/tmp";
if (!empty(\$_SERVER["FILE_TEMP_PATH"])) {
  \$settings["file_temp_path"] = \$_SERVER["FILE_TEMP_PATH"];
}

// Load environment settings file
if (file_exists(\$app_root . "/" . \$site_path . "/settings.".\$_SERVER["ENVIRONMENT"].".php")) {
  include \$app_root . "/" . \$site_path . "/settings.".\$_SERVER["ENVIRONMENT"].".php";
}

if (!empty(\$_SERVER["CONFIG_SYNC_FOLDER"])) {
  \$settings["config_sync_directory"] = \$_SERVER["CONFIG_SYNC_FOLDER"];
} else {
  \$settings["config_sync_directory"] = "../config/sync";
}

if (!empty($_SERVER["TRUSTED_HOST_WWW"])) {
  \$settings["trusted_host_patterns"] = [
    \$_SERVER["TRUSTED_HOST_WWW"],
    \$_SERVER["TRUSTED_HOST_NOWWW"]
  ];
}

EOF

echo "**********************"
echo "Enter Drupal as admin"
echo "**********************"
ddev drush uli

echo ""
echo ""
echo "**********************"
echo "Don't forget to move drupal hash_salt from settings.ddev.php to settings.php!!"
echo "**********************"

mkdir config
mkdir config/sync
rm d9.sh
