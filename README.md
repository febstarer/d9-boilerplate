# Drupal 9 Boilerplate script

D9BS is a script that automates repetitive tasks when installing a Drupal 9 website with DDEV

## Installation and usage

Clone this project in the desired folder
Enter the folder and run

```bash
./d9.sh
```

## What it does
It will only ask you the name of the project and then:

- creates a ddev project with a "my-project.test" extension
- installs the latest version of drupal 9 and some basic modules such as metatag, paragraphs, gin admin theme, field group...
- sets up load.environmments via dotenv file
- installs the website
- set gin as default admin theme

## Troubleshooting
if you get a bad interpreter error
```
./d9.sh: bad interpreter: /bin/sh^M:
```
please run

```
dos2unix d9.sh
```
